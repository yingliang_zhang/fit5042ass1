# This is my README
FlickPhotoBox
This is a HTML5 application using the JSF and Java EE 7 platform. The application serves as a frontend to the web services of Flickr.com. The web services are accessible via REST and are able to provide the data from a request in JSON format.
