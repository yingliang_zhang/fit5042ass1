/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import javax.el.ELContext;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;
import javax.faces.context.FacesContext;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.FlickrApi;
import org.scribe.model.Token;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

/**
 *
 * @author bboybeam
 */
@ManagedBean
@ApplicationScoped
public class FlickrApp {
    private static final String API_KEY = "b196d012875f46d880208e63a1614f58";
    private static final String API_SECRET = "2b647c81f96de8cf";
    private OAuthService service;
    //private Verifier verifier;
    private Token requestToken;
    private Token accessToken;
    private String authorizationUrl;
    
 
    /**
     * Creates a new instance of FlickrApp
     */
    public FlickrApp() {
           service = new ServiceBuilder()
                    .provider(FlickrApi.class)
                    .apiKey(API_KEY)
                    .apiSecret(API_SECRET)
                    .build();
    }
    
    public static String getApiKey() {
        return API_KEY;
    }

    public static String getApiSecret() {
        return API_SECRET;
    }

    public OAuthService getService() {
        return service;
    }

    public Token getRequestToken() {
        return requestToken;
    }

    public Token getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(Token accessToken) {
        this.accessToken = accessToken;
    }
    
    
    
    public String getAuthorizationUrl() {
        return authorizationUrl;
    }
    
    public static FlickrApp getInstance() {
        // Get application context bean via ExpressionLanguage
        ELContext context = FacesContext.getCurrentInstance().getELContext();
        
        // Get the current app instance via EL name "twitterApp"
        FlickrApp app = (FlickrApp) FacesContext.getCurrentInstance()
                    .getApplication()
                    .getELResolver().getValue(context, null, "flickrApp");
        
        // Return current instance
        return app;
    }
    
}
