
import java.io.StringReader;
import java.util.ArrayList;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Verb;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author bboybeam
 */
public class Photo {
    private String id;
    private String owner;
    private String secret;
    private String server;
    private int farm;
    private String title;
    private int ispublic;
    private int isfriend;
    private int isfamily;
    private String url;
    private String mediumPhotoUrl;
    private String smallPhotoUrl;
    private User user;
    private ArrayList<User> contacts;
    private static final String PROTECTED_RESOURCE_URL = "http://api.flickr.com/services/rest/?";

    public Photo(String id, String owner, String secret, String server, int farm, String title, int ispublic, int isfriend, int isfamily) {
        this.id = id;
        this.owner = owner;
        this.secret = secret;
        this.server = server;
        this.farm = farm;
        this.title = title;
        this.ispublic = ispublic;
        this.isfriend = isfriend;
        this.isfamily = isfamily;
        this.url = buildImgUrl(farm, server, id, secret);
        this.mediumPhotoUrl = buildMediumImgUrl(farm, server, id, secret);
        this.smallPhotoUrl = buildSmallImgUrl(farm, server, id, secret);
    }
    
    /**
     * Return the large size image link of the photo
     */
    public String buildImgUrl(int farm, String server, String id, String secret) {
        return "http://farm" + farm + ".staticflickr.com/" + server + "/" + id + "_" + secret +"_c.jpg";
    }
    
    /**
     * Return the medium size image link of the photo
     */
    public String buildMediumImgUrl (int farm, String server, String id, String secret) {
        return "http://farm" + farm + ".staticflickr.com/" + server + "/" + id + "_" + secret +"_n.jpg";
    }
    
    /**
     * Return the small size image link of the photo
     */
    public String buildSmallImgUrl (int farm, String server, String id, String secret) {
        return "http://farm" + farm + ".staticflickr.com/" + server + "/" + id + "_" + secret +"_s.jpg";
    }
    
    public void setContacts(ArrayList<User> contacts) {
        this.contacts = contacts;
    }
    
    /**
     * Return the user of a photo base on its owner id.
     */
    public User getPhotoUser() {
        FlickrApp app = FlickrApp.getInstance();
        String realname;
        String location;
        OAuthRequest request = new OAuthRequest(Verb.GET, PROTECTED_RESOURCE_URL);
        
        request.addQuerystringParameter("method", "flickr.people.getInfo");
        request.addQuerystringParameter("api_key",FlickrApp.getApiKey());
        request.addQuerystringParameter("user_id", owner);
        request.addQuerystringParameter("format", "json");
        request.addQuerystringParameter("nojsoncallback", "1");
        Response response = request.send();
        
        
        JsonObject json = Json.createReader(new StringReader(response.getBody())).readObject();
        
        JsonObject person = json.getJsonObject("person");
        
        // Get the required information needed
        String nsid = person.getString("nsid");
        String iconserver = person.getString("iconserver");
        int iconfarm = person.getInt("iconfarm");
        
        JsonObject userName = person.getJsonObject("username");
        String username = userName.getString("_content");
        
        JsonObject realName = person.getJsonObject("realname");
        if(realName == null){
            realname = "";
        }else{
            realname = realName.getString("_content");
        }
        
        JsonObject userLocation = person.getJsonObject("location");
        if(userLocation == null){
            location = "";
        }else{
            location = userLocation.getString("_content");
        }
        
        JsonObject desc = person.getJsonObject("description");
        String description = desc.getString("_content");
        
        JsonObject photosUrl = person.getJsonObject("photosurl");
        String photosurl = photosUrl.getString("_content");
        
        JsonObject profileUrl = person.getJsonObject("profileurl");
        String profileurl = profileUrl.getString("_content");

        // Create the User object
        user = new User(nsid, iconfarm, iconserver, username, realname, location, description, photosurl, profileurl);
        
        return user;
    }
    
    
    /**
     * Return an array of contact details of a certain user based on the user's id.
     */
    public ArrayList<User> getContacts() {
        int resultSize = 0;
        FlickrApp app = FlickrApp.getInstance();
        
        OAuthRequest request = new OAuthRequest(Verb.GET, PROTECTED_RESOURCE_URL);
        
        request.addQuerystringParameter("method", "flickr.contacts.getPublicList");
        request.addQuerystringParameter("api_key",FlickrApp.getApiKey());
        request.addQuerystringParameter("user_id", owner);
        request.addQuerystringParameter("format", "json");
        request.addQuerystringParameter("nojsoncallback", "1");
        Response response = request.send();
        
        // Create a list to hold results (if any)
        contacts = new ArrayList<User>();
        
        JsonObject json = Json.createReader(new StringReader(response.getBody())).readObject();
        
        JsonObject result = json.getJsonObject("contacts");
        
        if(result.getJsonArray("contact") != null){
            resultSize = result.getJsonArray("contact").size();
            // If the the result size is than 10, get the top 10 objects, else get all the objects.
            if(resultSize > 10){
                for (int i=0; i < 10; i++) {
            
                    // Create a new Contact object for each result
                    JsonObject contactDetails = result.getJsonArray("contact").getJsonObject(i);

                    // Get the required information needed
                    String contactid = contactDetails.getString("nsid");
                    String contactname = contactDetails.getString("username");
                    int iconfarm = contactDetails.getInt("iconfarm");
                    String iconserver = contactDetails.getString("iconserver");

                    // Create the Photo object
                    User u = new User(contactid, iconfarm, iconserver, contactname);

                    // And add to our container
                    contacts.add(u);
                }
            }else{
                for (int i=0; i < resultSize; i++) {
            
                    // Create a new Contact object for each result
                    JsonObject contactDetails = result.getJsonArray("contact").getJsonObject(i);
                    
                    // Get the required information needed
                    String contactid = contactDetails.getString("nsid");
                    String contactname = contactDetails.getString("username");
                    int iconfarm = contactDetails.getInt("iconfarm");
                    String iconserver = contactDetails.getString("iconserver");

                    // Create the Contact object
                    User u = new User(contactid, iconfarm, iconserver, contactname);

                    // And add to our container
                    contacts.add(u);
                }
            }
            
            return contacts;
        }else {
            return contacts; 
        }
    }
    
    
    

    public String getId() {
        return id;
    }

    public String getOwner() {
        return owner;
    }

    public String getSecret() {
        return secret;
    }

    public String getServer() {
        return server;
    }

    public int getFarm() {
        return farm;
    }

    public String getTitle() {
        return title;
    }

    public int getIspublic() {
        return ispublic;
    }

    public int getIsfriend() {
        return isfriend;
    }

    public int getIsfamily() {
        return isfamily;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMediumPhotoUrl() {
        return mediumPhotoUrl;
    }

    public void setMediumPhotoUrl(String mediumPhotoUrl) {
        this.mediumPhotoUrl = mediumPhotoUrl;
    }

    public String getSmallPhotoUrl() {
        return smallPhotoUrl;
    }

    public void setSmallPhotoUrl(String smallPhotoUrl) {
        this.smallPhotoUrl = smallPhotoUrl;
    }

    public User getUser() {
        return user;
    }
    
    
    

    
}
