/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.StringReader;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.json.Json;
import javax.json.JsonObject;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Verb;

/**
 *
 * @author bboybeam
 */
@ManagedBean
@RequestScoped
public class indexController {
    private String searchQuery = "";
    private ArrayList<Photo> photos;
    private ArrayList<Photo> searchedPhotos;
    private Photo p1;
    private Photo p2;
    private Photo p3;
    private Photo p4;
    private Photo p5;
    
    
    private static final String PROTECTED_RESOURCE_URL = "http://api.flickr.com/services/rest/?";
    /**
     * Creates a new instance of indexController
     */
    public indexController() {
        searchQuery = "";
    }

    public void setPhotos(ArrayList<Photo> photos) {
        this.photos = photos;
    }

    public ArrayList<Photo> getPhotos() {
        return photos;
    }

    public void setSearchedPhotos(ArrayList<Photo> searchPhotos) {
        this.searchedPhotos = searchPhotos;
    }

    public ArrayList<Photo> getSearchedPhotos() {
        return searchedPhotos;
    }
    

    
    /**
     * Return an array of recent photos.
     */
    public ArrayList<Photo> getRecentPhotos() {
        FlickrApp app = FlickrApp.getInstance();
        
        OAuthRequest request = new OAuthRequest(Verb.GET, PROTECTED_RESOURCE_URL);
        
        request.addQuerystringParameter("method", "flickr.photos.getRecent");
        request.addQuerystringParameter("api_key",FlickrApp.getApiKey());
        request.addQuerystringParameter("format", "json");
        request.addQuerystringParameter("nojsoncallback", "1");
        Response response = request.send();
        
        
        JsonObject json = Json.createReader(new StringReader(response.getBody())).readObject();
        
        JsonObject result = json.getJsonObject("photos");
        
        // Create a list to hold results (if any)
        photos = new ArrayList<Photo>();
        
        for (int i=0; i < 6; i++) {
            
            // Create a new photo object for each result
            JsonObject photoDetails = result.getJsonArray("photo").getJsonObject(i);
            
            // Get the required information needed
            String id = photoDetails.getString("id");
            String owner = photoDetails.getString("owner");
            String secret = photoDetails.getString("secret");
            String server = photoDetails.getString("server");
            int farm = photoDetails.getInt("farm");
            String title = photoDetails.getString("title");
            int ispublic = photoDetails.getInt("ispublic");
            int isfriend = photoDetails.getInt("isfriend");
            int isfamily = photoDetails.getInt("isfamily");

            // Create the Photo object
            Photo p = new Photo(id, owner, secret, server, farm, title, ispublic, isfriend, isfamily);
            
            // And add to our container
            photos.add(p);
        }
            p1 = photos.get(0);
            p2 = photos.get(1);
            p3 = photos.get(2);
            p4 = photos.get(3);
            p5 = photos.get(4);
            return photos;
    }
    
    
    /**
     * Return an array of photos based on user's query
     */
     public ArrayList<Photo> getSearchPhotos() {
         if (searchQuery.equals("")) {
            // Return blank arraylist if blank search query
            return new ArrayList<Photo>();
        }else {
        FlickrApp app = FlickrApp.getInstance();
        

        OAuthRequest request = new OAuthRequest(Verb.GET, PROTECTED_RESOURCE_URL);
        
        request.addQuerystringParameter("method", "flickr.photos.search");
        request.addQuerystringParameter("api_key",FlickrApp.getApiKey());
        request.addQuerystringParameter("text", searchQuery);
        request.addQuerystringParameter("format", "json");
        request.addQuerystringParameter("nojsoncallback", "1");
        Response response = request.send();

        
        JsonObject json = Json.createReader(new StringReader(response.getBody())).readObject();
        
        JsonObject result = json.getJsonObject("photos");
        int resultSize = result.getJsonArray("photo").size();
        
        // Create a list to hold results (if any)
        searchedPhotos = new ArrayList<Photo>();
        
        for (int i=0; i < resultSize; i++) {
            
            // Create a new photo object for each result
            JsonObject photoDetails = result.getJsonArray("photo").getJsonObject(i);
            
            // Get the required information needed
            String id = photoDetails.getString("id");
            String owner = photoDetails.getString("owner");
            String secret = photoDetails.getString("secret");
            String server = photoDetails.getString("server");
            int farm = photoDetails.getInt("farm");
            String title = photoDetails.getString("title");
            if(title.length() >= 20) {
                title = title.substring(0, 20);
            }
            int ispublic = photoDetails.getInt("ispublic");
            int isfriend = photoDetails.getInt("isfriend");
            int isfamily = photoDetails.getInt("isfamily");

            // Create the Photo object
            Photo p = new Photo(id, owner, secret, server, farm, title, ispublic, isfriend, isfamily);
            
            
            
            // And add to our container
            searchedPhotos.add(p);
        }
            return searchedPhotos;
        }
    }

    public void setSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
    }
    
    public String getSearchQuery() {
        return searchQuery;
    }
    
    public Photo getP1() {
        return p1;
    }

    public Photo getP2() {
        return p2;
    }

    public Photo getP3() {
        return p3;
    }

    public Photo getP4() {
        return p4;
    }

    public Photo getP5() {
        return p5;
    }
    
    
}
