/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.StringReader;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.json.Json;
import javax.json.JsonObject;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Verb;

/**
 *
 * @author bboybeam
 */
@ManagedBean
@RequestScoped
public class placesController {
    private ArrayList<Place> places;
    private Place place;
    private String searchQuery = "";
    private static final String PROTECTED_RESOURCE_URL = "http://api.flickr.com/services/rest/?";
    /**
     * Creates a new instance of placesController
     */
    public placesController() {
        searchQuery = "";
    }

    public void setPlaces(ArrayList<Place> places) {
        this.places = places;
    }

    
    /**
     * Return an array of places based on user's query
     */
    public ArrayList<Place> getPlaces() {
         if (searchQuery.equals("")) {
            // Return blank arraylist if blank search query
            return new ArrayList<Place>();
        }else {
        FlickrApp app = FlickrApp.getInstance();
        

        OAuthRequest request = new OAuthRequest(Verb.GET, PROTECTED_RESOURCE_URL);
        
        request.addQuerystringParameter("method", "flickr.places.find");
        request.addQuerystringParameter("api_key",FlickrApp.getApiKey());
        request.addQuerystringParameter("query", searchQuery);
        request.addQuerystringParameter("format", "json");
        request.addQuerystringParameter("nojsoncallback", "1");
        Response response = request.send();

        
        JsonObject json = Json.createReader(new StringReader(response.getBody())).readObject();
        
        JsonObject result = json.getJsonObject("places");
        
        // Create a list to hold results (if any)
        places = new ArrayList<Place>();     
        // Create a new photo object for each result
        JsonObject placeDetails = result.getJsonArray("place").getJsonObject(0);

        // Get the required information needed
        String placeid = placeDetails.getString("place_id");
        String woeid = placeDetails.getString("woeid");
        String latitude = placeDetails.getString("latitude");
        String longitude = placeDetails.getString("longitude");
        String placeUrl = placeDetails.getString("place_url");
        String placeType = placeDetails.getString("place_type");
        String placeTypeId = placeDetails.getString("place_type_id");
        String timezone = placeDetails.getString("timezone");
        String content = placeDetails.getString("_content");
        String woeName = placeDetails.getString("woe_name");
        
        // Create the Photo object
        Place p = new Place(placeid, woeid, latitude, longitude, placeUrl, placeType, placeTypeId, timezone, content, woeName);


        // And add to our container
        places.add(p);
        }
        place = places.get(0);
        return places;
    }

    public Place getPlace() {
        return place;
    }

    public String getSearchQuery() {
        return searchQuery;
    }

    public void setSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
    }
    
    
}
