
import java.io.StringReader;
import java.util.ArrayList;
import javax.json.Json;
import javax.json.JsonObject;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Verb;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author bboybeam
 */
public class User {
    private String nsid;
    private int iconFarm;
    private String iconServer;
    private String userName;
    private String realName;
    private String location;
    private String description;
    private String photosUrl;
    private String profileUrl;
    private String buddyIconUrl; 
    private ArrayList<Photo> photos;

    private static final String PROTECTED_RESOURCE_URL = "http://api.flickr.com/services/rest/?";
    
    public User(String nsid, int iconFarm, String iconServer, String userName, String realName, String location, String description, String photosUrl, String profileUrl) {
        this.nsid = nsid;
        this.iconFarm = iconFarm;
        this.iconServer = iconServer;
        this.userName = userName;
        this.realName = realName;
        this.location = location;
        this.description = description;
        this.photosUrl = photosUrl;
        this.profileUrl = profileUrl;
        this.buddyIconUrl = buildIconUrl(iconFarm, iconServer, nsid);
    }
    
    public User(String nsid, int iconFarm, String iconServer, String userName, String description, String photosUrl, String profileUrl) {
        this.nsid = nsid;
        this.iconFarm = iconFarm;
        this.iconServer = iconServer;
        this.userName = userName;
        this.description = description;
        this.photosUrl = photosUrl;
        this.profileUrl = profileUrl;
        this.buddyIconUrl = buildIconUrl(iconFarm, iconServer, nsid);
    }
    
    public User(String nsid, int iconFarm, String iconServer, String userName) {
        this.nsid = nsid;
        this.iconFarm = iconFarm;
        this.iconServer = iconServer;
        this.userName = userName;
        this.buddyIconUrl = buildIconUrl(iconFarm, iconServer, nsid);
    }
    
    public void setPhotos(ArrayList<Photo> photos) {
        this.photos = photos;
    }
    
    public String buildIconUrl(int iconFarm, String iconServer, String nsid) {
        String iconUrl = "";
        if (iconServer.equals("0") ) {
            iconUrl = "http://www.flickr.com/images/buddyicon.gif";
        } else {
            iconUrl = "http://farm" + iconFarm + ".staticflickr.com/" + iconServer + "/buddyicons/" + nsid + ".jpg";
        }
        return iconUrl;
    }
    
    
    public ArrayList<Photo> getPhotos() {
        int resultSize = 0;
        FlickrApp app = FlickrApp.getInstance();
        
        OAuthRequest request = new OAuthRequest(Verb.GET, PROTECTED_RESOURCE_URL);
        
        request.addQuerystringParameter("method", "flickr.people.getPublicPhotos");
        request.addQuerystringParameter("api_key",FlickrApp.getApiKey());
        request.addQuerystringParameter("user_id", nsid);
        request.addQuerystringParameter("format", "json");
        request.addQuerystringParameter("nojsoncallback", "1");
        Response response = request.send();
        
        // Create a list to hold results (if any)
        photos = new ArrayList<Photo>();
        
        JsonObject json = Json.createReader(new StringReader(response.getBody())).readObject();
        
        JsonObject result = json.getJsonObject("photos");
        
        if(result.getJsonArray("photo") != null){
            resultSize = result.getJsonArray("photo").size();
            if(resultSize > 10){
                for (int i=0; i < 10; i++) {
            
                    // Create a new Tweet object for each result
                    JsonObject photoDetails = result.getJsonArray("photo").getJsonObject(i);
                    
                    // Get the required information needed
                    String id = photoDetails.getString("id");
                    String owner = photoDetails.getString("owner");
                    String secret = photoDetails.getString("secret");
                    String server = photoDetails.getString("server");
                    int farm = photoDetails.getInt("farm");
                    String title = photoDetails.getString("title");
                    int ispublic = photoDetails.getInt("ispublic");
                    int isfriend = photoDetails.getInt("isfriend");
                    int isfamily = photoDetails.getInt("isfamily");

                    // Create the Photo object
                    Photo p = new Photo(id, owner, secret, server, farm, title, ispublic, isfriend, isfamily);

                    // And add to our container
                    photos.add(p);
                }
            }else{
                for (int i=0; i < resultSize; i++) {
            
                    // Create a new Tweet object for each result
                    JsonObject photoDetails = result.getJsonArray("photo").getJsonObject(i);

                    // Get the required information needed
                    String id = photoDetails.getString("id");
                    String owner = photoDetails.getString("owner");
                    String secret = photoDetails.getString("secret");
                    String server = photoDetails.getString("server");
                    int farm = photoDetails.getInt("farm");
                    String title = photoDetails.getString("title");
                    int ispublic = photoDetails.getInt("ispublic");
                    int isfriend = photoDetails.getInt("isfriend");
                    int isfamily = photoDetails.getInt("isfamily");

                    // Create the Photo object
                    Photo p = new Photo(id, owner, secret, server, farm, title, ispublic, isfriend, isfamily);

                    // And add to our container
                    photos.add(p);
                }
            }
            
            return photos;
        }else {
            return photos; 
        }
    }
    

    public String getNsid() {
        return nsid;
    }

    public int getIconFarm() {
        return iconFarm;
    }

    public String getIconServer() {
        return iconServer;
    }

    public String getUserName() {
        return userName;
    }

    public String getRealName() {
        return realName;
    }

    public String getLocation() {
        return location;
    }
    
    public String getDescription() {
        return description;
    }

    public String getPhotosUrl() {
        return photosUrl;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public String getBuddyIconUrl() {
        return buddyIconUrl;
    }

    public void setBuddyIconUrl(String buddyIconUrl) {
        this.buddyIconUrl = buddyIconUrl;
    }
    
    
}


