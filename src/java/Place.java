
import java.io.StringReader;
import java.util.ArrayList;
import javax.json.Json;
import javax.json.JsonObject;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Verb;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author bboybeam
 */
public class Place {
    private String placeId;
    private String weoid;
    private String latitude;
    private String longitude;
    private String place_url;
    private String place_type;
    private String place_type_id;
    private String timezone;
    private String content;
    private String woe_name;
    private ArrayList<Photo> photos;
    private static final String PROTECTED_RESOURCE_URL = "http://api.flickr.com/services/rest/?";
    
    public Place(String placeId, String weoid, String latitude, String longitude, String place_url, String place_type, String place_type_id, String timezone, String content, String woe_name) {
        this.placeId = placeId;
        this.weoid = weoid;
        this.latitude = latitude;
        this.longitude = longitude;
        this.place_url = place_url;
        this.place_type = place_type;
        this.place_type_id = place_type_id;
        this.timezone = timezone;
        this.content = content;
        this.woe_name = woe_name;
    }
    
    public void setPhotos(ArrayList<Photo> photos) {
        this.photos = photos;
    }

    public ArrayList<Photo> getPhotos() {
        return photos;
    }
    
    
    /**
     * Get photos were taken around the place according to the latitude and longitude.
     */
    public ArrayList<Photo> getPlacePhotos() {
        int resultSize = 0;
        FlickrApp app = FlickrApp.getInstance();
        
        OAuthRequest request = new OAuthRequest(Verb.GET, PROTECTED_RESOURCE_URL);
        
        request.addQuerystringParameter("method", "flickr.photos.search");
        request.addQuerystringParameter("api_key",FlickrApp.getApiKey());
        request.addQuerystringParameter("lat", latitude);
        request.addQuerystringParameter("lon", longitude);
        request.addQuerystringParameter("format", "json");
        request.addQuerystringParameter("nojsoncallback", "1");
        Response response = request.send();
        
        // Create a list to hold results (if any)
        photos = new ArrayList<Photo>();
        
        JsonObject json = Json.createReader(new StringReader(response.getBody())).readObject();
        
        // Get the json object photos
        JsonObject result = json.getJsonObject("photos");
        
        if(result.getJsonArray("photo") != null){
            resultSize = result.getJsonArray("photo").size();
            if(resultSize > 20){
                for (int i=0; i < 20; i++) {
            
                    // Create a new photo object for each result
                    JsonObject photoDetails = result.getJsonArray("photo").getJsonObject(i);

                    // Get the required photo information needed
                    String id = photoDetails.getString("id");
                    String owner = photoDetails.getString("owner");
                    String secret = photoDetails.getString("secret");
                    String server = photoDetails.getString("server");
                    int farm = photoDetails.getInt("farm");
                    String title = photoDetails.getString("title");
                    int ispublic = photoDetails.getInt("ispublic");
                    int isfriend = photoDetails.getInt("isfriend");
                    int isfamily = photoDetails.getInt("isfamily");

                    // Create the Photo object
                    Photo p = new Photo(id, owner, secret, server, farm, title, ispublic, isfriend, isfamily);
            
                    // And add to the container
                    photos.add(p);
                }
            }else{
                for (int i=0; i < resultSize; i++) {
            
                    // Create a new photo object for each result
                    JsonObject photoDetails = result.getJsonArray("photo").getJsonObject(i);

                    // Get the required information needed
                    String id = photoDetails.getString("id");
                    String owner = photoDetails.getString("owner");
                    String secret = photoDetails.getString("secret");
                    String server = photoDetails.getString("server");
                    int farm = photoDetails.getInt("farm");
                    String title = photoDetails.getString("title");
                    int ispublic = photoDetails.getInt("ispublic");
                    int isfriend = photoDetails.getInt("isfriend");
                    int isfamily = photoDetails.getInt("isfamily");
                    
                    // Create the Photo object
                    Photo p = new Photo(id, owner, secret, server, farm, title, ispublic, isfriend, isfamily);
            
                    // And add to our container
                    photos.add(p);
                }
            }
            
            return photos;
        }else {
            return photos; 
        }
    }

    public String getPlaceId() {
        return placeId;
    }

    public String getWeoid() {
        return weoid;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getPlace_url() {
        return place_url;
    }

    public String getPlace_type() {
        return place_type;
    }

    public String getPlace_type_id() {
        return place_type_id;
    }

    public String getTimezone() {
        return timezone;
    }

    public String getContent() {
        return content;
    }

    public String getWoe_name() {
        return woe_name;
    }
    
}
