/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
	setInterval("rotateImages()", 4000);
        
        $(".fancybox-thumb").fancybox({
		prevEffect	: 'none',
		nextEffect	: 'none',
		helpers	: {
			title	: {
				type: 'outside'
			},
			thumbs	: {
				width	: 50,
				height	: 50
			}
		}
	});
        
        $('a.darken').hover(function(){
            $(this).find('img').fadeTo(500, 0.5);
        },function() {
            $(this).find('img').fadeTo(500, 1);
        });
  
});

function rotateImages() {
        var oCurPhoto = $("#slider div.current");
        var oNxtPhoto = oCurPhoto.next("div.next");

        if (oNxtPhoto.length === 0) {
                oNxtPhoto = $("#slider div:first");
        };

        oCurPhoto.removeClass("current").addClass("previous");
        oNxtPhoto.css({opacity: 0.0}).addClass("current").animate({opacity: 1.0}, 1000, 
                function() {
                        oCurPhoto.removeClass("previous");
                });
}

function darkenImages(data){
    if (data.status == 'success'){
        $('a.darken').hover(function(){
            $(this).find('img').fadeTo(500, 0.5);
        },function() {
            $(this).find('img').fadeTo(500, 1);
        });
    }  
}


